"""@file        mcp9808.py 
@brief          Holds class for mcp9808 data logging over i2c
@details        (Hosted at https://bitbucket.org/Miknoian/me-405/src/master/Lab/Lab4/mcp9808.py)
@author         Mark Iknoian. 
@date           5/13/21
"""

from pyb import I2C
import utime
import struct

class mcp9808():
    """ @brief          Class for interfacing with mcp9808 over i2c
        @details        Uses i2c to return ambient temps and manufacturer ID
    """
    def __init__(self, i2cObject, i2cAddress):
        ''' @brief              Constructs a mcp9808.mcp9808 object
            @details            Takes existing i2c object and desired address to 
                                create mcp9808 object
            @param i2cObject    Existing i2c object for sensor
            @param i2cAddress   Address of i2c board
        ''' 
        self.i2c_9808 = i2cObject
        
        self.i2cAddress = i2cAddress
        
        # empty array for upper and lower bytes from register
        self.dataArray = bytearray([0,0])
    
    def check(self):
        ''' @brief          Checks and verifies manufacturer ID
            @details        Compares returned hexadecimal of ID to expected 0x54 value
            @return         Poisitive or negative result of ID match attempt
        ''' 
        
        # read register 6
        self.i2c_9808.mem_read(self.dataArray, self.i2cAddress, 6)
        
        # find and convert ID to hev
        self.manufID = hex(self.dataArray[0] + self.dataArray[1])
        
        # expected ID
        if self.manufID == hex(0x54):
            return 'ID Match! Setup Successful!'
        else:
            return 'ID Mismatch! Verify Setup!'
                 
    def celsius(self):
        ''' @brief              Returns ambient room temperature in celsius
            @return             Temperature in celsius
        ''' 
        
        # read register 5
        self.i2c_9808.mem_read(self.dataArray, self.i2cAddress, 5)
        
        # from mcp9808 docs
        self.upperByte = self.dataArray[0] & 0x1F
        self.lowerByte = self.dataArray[1]
        self.tempCelsius = (self.upperByte*16) + (self.lowerByte/16)
        return self.tempCelsius
        
        
    def farhenheit(self):
        ''' @brief              Returns ambient room temperature in farhenheit
            @return             Temperature in farhenheit
        '''
        
        # same as celsius read but with conversion at end
        self.i2c_9808.mem_read(self.dataArray, self.i2cAddress, 5)
        self.upperByte = self.dataArray[0] & 0x1F
        self.lowerByte = self.dataArray[1]
        self.tempFarhenheit = ((self.upperByte*16) + (self.lowerByte/16))*1.8 + 32
        return self.tempFarhenheit


# test code that only runs when file is 'main.py'
if __name__=='__main__':
    
    # create i2c object
    i2c_9808 = I2C(1, I2C.MASTER)
    
    # create mcp9808 object
    tempSensor = mcp9808(i2c_9808, 0x18)

    while True:
        
        # test all methods
        cTemp = tempSensor.celsius()
        fTemp = tempSensor.farhenheit()
        IDcheck = tempSensor.check()
        
        utime.sleep(1) 
        print(cTemp)
        print(fTemp)
        print(IDcheck)
        





