'''@file        405_lab4.py
@brief          Samples temperature from STM32 and MCP 9808
@details        Main program loop to call and execute mcp9808.py to log temperature data.
                Writes this data along with recorded STM32 temps to .csv
                after c-trl c is pressed.
                (Hosted at https://bitbucket.org/Miknoian/me-405/src/master/Lab/Lab4/405_lab4.py)
                (Data available at https://bitbucket.org/Miknoian/me-405/src/master/Lab/Lab4/dataOut.csv)
@author         Mark Iknoian. 
@date           5/13/21
@image          html lab4_data.png "Resulting data from 8 hour logging period"
'''

import micropython
import utime
import mcp9808
from pyb import I2C
import pyb
import array

if __name__ == "__main__":  #only run as main program

    # i2c object creation
    i2c_9808 = I2C(1, I2C.MASTER)

    # mcp9808 object creation
    mcpTempClass = mcp9808.mcp9808(i2c_9808, 0x18)  # call simonTask 
    
    # ADCAll object creation for STM32 temp
    nucleoTempClass = pyb.ADCAll(12,0x70000)
    
    # define ref voltage for ADC
    nucleoTempClass.read_core_vref()
    
    micropython.alloc_emergency_exception_buf(200) # memory for error handler
    
    # empty arrays for data collection
    time = array.array('f')
    mcpData = array.array('f')
    nucleoData = array.array('f')
    
    startTime = utime.ticks_ms()

    while True:     # loop FSM calls until interrupt
        try:
            # read mcp9808 temp in farhenheit
            mcpTemp = mcpTempClass.farhenheit()
            
            # read stm32 temp and convert to farhenheit
            nucleoTemp = nucleoTempClass.read_core_temp()*1.8 + 32
            
            # current time stamp in ms from start
            currentTime = utime.ticks_diff(utime.ticks_ms(),startTime)
            
            # add all current values to arrays
            mcpData.append(float(mcpTemp))
            nucleoData.append(float(nucleoTemp))
            time.append(float(currentTime))
            
            # let user know system status
            print('STM32 Temp: ' + str(mcpTemp))
            print('Nucleo Temp: ' + str(nucleoTemp))
            print('Current Time (min): ' + str(currentTime/60000))
            
            # wait 1 min for data collection
            utime.sleep(60)

            
            
        except KeyboardInterrupt:   # on interrupt break loop and display wins/losses
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            
            # iterate through array and print to .csv
            with open ("dataOut.csv", "w") as file:
                for n in range(len(mcpData)):
                    
                    # one decimal precision for temps, none for time
                    file.write("{:.1f},{:.1f},{:f}\r\n".format(mcpData[n],nucleoData[n],time[n]))
                 
            print('Ctrl-c detected. Data has been logged. Goodbye')
            break