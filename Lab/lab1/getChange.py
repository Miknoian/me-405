'''@file        getChange.py
@brief          Calculates change to provide based on price/payment
@details        Checks user payment input and subtracts price from total,
                The ammount owed is returned as change that favors the largest
                currency denomination first.
                (Hosted at https://bitbucket.org/Miknoian/me-405/src/master/Lab/lab1/getChange.py)
@author         Mark Iknoian
@date           4/25/21
'''
def getChange(price, payment):
    '''
    @brief              This function returns change based on price/payment
    @param price        Price of item in cents
    @param payment      List of provided payment in order of 1,5,10,25,100,500,1000,2000 cents
    @return             Change in matching list format
    ''' 
    
    # defines value of each list position
    refAmount = [1, 5, 10, 25, 100, 500, 1000, 2000]
    
    # empty list for change
    change = [0, 0, 0, 0, 0, 0, 0, 0]
    
    # define values used later
    total = 0
    balOwe = 0
    
    # iterates through user payment input to calc total in cents
    n = 0
    for n in range(8):
        total =+ (total + refAmount[n]*payment[n])
        n =+ 1
    
    # if total is equal to or less than the price an empty list is returned
    if total <= price:
        return 8*[0]
    
    # if total is greater than price change is calculated
    elif total > price:    
        balOwe = total - price
        
        # define variables for loop
        chngProc = 0
        chngPos = 7
        
        # check if dividing the ammount owed by refAmount is greater than zero.
        # if so, the current refAmount can be used for payment by rounding the
        # div result down to an int. Loop then iterates for each denomination
        # until balOwe = 0
        
        while chngProc == 0:
                if balOwe/int(refAmount[chngPos]) >= 1:
                    change[chngPos] = int(balOwe/refAmount[chngPos])
                    balOwe = balOwe - int(balOwe/refAmount[chngPos])*refAmount[chngPos]
                    if balOwe == 0:
                        return change
                else:
                    chngPos = chngPos - 1