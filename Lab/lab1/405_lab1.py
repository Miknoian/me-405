"""@file        405_lab1.py 
@brief          Simulation of soda machine
@details        Simulates payment/UI/dispensing of soda machine. Uses generator to advance each state of a FSM.
                Requires getTotal.py and getChange.py.
                (Hosted at https://bitbucket.org/Miknoian/me-405/src/master/Lab/lab1/405_lab1.py)
@author         Mark Iknoian. 
@date           4/25/21
@image          html 405_lab1_FSM.png "Program FSM"
"""

from time import sleep
from getChange import getChange
from getTotal import getTotal
import keyboard
import time
global buttonCoin
global buttonDrink

def VendotronTask():
    """ @brief          Implements FSM for soda machine simulation
        @details        Takes user keyboard inputs from main program to
                        to interact with FSM. Function can handle payment options,
                        dispense change, and "dispense" drinks.
    """
    
    # init state
    state = 0
    
    # log start time for attract message
    startTime = time.time()
    
    # time in seconds w/ no user input required to display attract message
    idleModeDelay = 20
    
    # global vars shared between input loop/function
    global last_key
    global buttonCoin
    global buttonDrink
    global payment
    
    # used for input type check
    buttonCoin = 0
    buttonDrink = 0
    
    # dictionaries for soda price/type
    itemPrice = {'c':100, 'p':120, 's':85, 'd':110}
    itemName = {'c':'CUKE', 'p':'POPSY', 's':'SPRYTE', 'd':'DR. PUPPER'} 
    
    while True:
        # Implement FSM using a while loop and an if statement
        # will run eternally until user presses CTRL-C
         
        if state == 0:
            # perform state 0 operations
            # this init state, initializes the FSM itself, with all the
            # code features already set up
            
            print("INITIALIZING...VER 4.51")
            print("PLEASE INSERT PAYMENT")
            state = 1       # on the next iteration, the FSM will run state 1
            
        elif state == 1:
            # perform state 1 operations
            idleModeCount = time.time()
            
            # check for payment related input
            if buttonCoin == 1:
                buttonCoin = 0
                last_key = ''
                
                # print current total
                print("{:.2f}".format(getTotal(payment)))
                
                # reset attract message counter
                startTime = time.time()
                
            # check for drink related input
            elif buttonDrink == 1:
                
                # advance to state 2 for next iteration
                state = 2
            
            # time check to display attract message
            elif idleModeCount - startTime >= idleModeDelay:
                
                    # reset attract message counter
                    startTime = time.time()
                    print("BUY DRINK...OR ELSE")

        elif state == 2:
            # perform state 2 operations
            
            # reset drink input check
            buttonDrink = 0
            
            # get cost from dictionary
            cost = itemPrice[soda]

            # payment insufficient
            if cost > getTotal(payment)*100:
                print('PAYMENT INSUFFICIENT')
                state = 1
                
            # payment okay
            elif cost <= (getTotal(payment)*100):
                print(itemName[soda], 'DISPENSED, ENJOY IMMEDIATELY')
                payment = getChange(cost,payment)
                
                # offer second beverage if payment remains
                if (getTotal(payment)*100) != 0:
                    print('PLEASE SELECT ANOTHER BEVERAGE')
                    state = 1
                else:
                    state = 1
            
        else:
            # this state shouldn't exist!
            pass
        
        # stop until next call
        yield(state)
        
def kb_cb(key):
    ''' @brief          Callback function for keyboard input
        @details        Takes user key input used for FSM
        @param key      User key input
    '''
    global last_key
    last_key = key.name

if __name__ == "__main__":
    # Initialize code to run FSM (not the init state). This means building the 
    # iterator as generator object

    # define generator to iterate through
    vendo = VendotronTask()
    global payment
    
    # payment structure
    payment = [0, 0, 0, 0, 0, 0, 0, 0]

    
    # Run the task every 10ms (approximately) unless we get a ctrl-c or the
    # task stops yielding.
    
    # Tell the keyboard module to respond to these particular keys only
    keyboard.on_release_key("e", callback=kb_cb)
    keyboard.on_release_key("c", callback=kb_cb)
    keyboard.on_release_key("p", callback=kb_cb)
    keyboard.on_release_key("s", callback=kb_cb)
    keyboard.on_release_key("d", callback=kb_cb)
    keyboard.on_release_key("q", callback=kb_cb)
    keyboard.on_release_key("0", callback=kb_cb)
    keyboard.on_release_key("1", callback=kb_cb)
    keyboard.on_release_key("2", callback=kb_cb)
    keyboard.on_release_key("3", callback=kb_cb)
    keyboard.on_release_key("4", callback=kb_cb)
    keyboard.on_release_key("5", callback=kb_cb)
    keyboard.on_release_key("6", callback=kb_cb)
    keyboard.on_release_key("7", callback=kb_cb)
    
    # reset key input
    last_key = ''

    try:
        while True:
            
            # iterate through FSM once
            next(vendo) 
            sleep(0.01)
            
            # check key input provided by callback function
            if last_key == 'e':
                last_key = ''
                print('PAYMENT EJECTED')
                payment = [0, 0, 0, 0, 0, 0, 0, 0]
            elif last_key == 'c':
                last_key = ''
                soda = 'c'
                buttonDrink = 1
            elif last_key == 'p':
                last_key = ''
                soda = 'p'
                buttonDrink = 1
            elif last_key == 's':
                last_key = ''
                soda = 's'
                buttonDrink = 1
            elif last_key == 'd':
                last_key = ''
                soda = 'd'
                buttonDrink = 1
            elif last_key == '0':
                payment[0] = payment[0] + 1
                buttonCoin = 1
            elif last_key == '1':
                payment[1] = payment[1] + 1
                buttonCoin = 1                
            elif last_key == '2':
                payment[2] = payment[2] + 1   
                buttonCoin = 1
            elif last_key == '3':
                payment[3] = payment[3] + 1
                buttonCoin = 1
            elif last_key == '4':
                payment[4] = payment[4] + 1
                buttonCoin = 1
            elif last_key == '5':
                payment[5] = payment[5] + 1
                buttonCoin = 1
            elif last_key == '6':
                payment[6] = payment[6] + 1
                buttonCoin = 1
            elif last_key == '7':
                payment[7] = payment[7] + 1
                buttonCoin = 1
            elif last_key == 'q':
                print("'q' detected. Quitting...")
                keyboard.unhook_all ()
                break            
            
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
            
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')