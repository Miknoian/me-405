'''@file        getTotal.py
@brief          Calculates total based on payment list
@details        Evaluates payment list and multiplies current currency values
                by ammount to return total 
                (Hosted at https://bitbucket.org/Miknoian/me-405/src/master/Lab/lab1/getTotal.py)
@author         Mark Iknoian
@date           4/25/21
'''
def getTotal(payment):
    '''
    @brief              This function returns total based on payment list
    @param payment      List of provided payment in order of 1,5,10,25,100,500,1000,2000 cents
    @return             Total in # of pennies
    ''' 
    
    # defines value of each list position
    refAmount = [1, 5, 10, 25, 100, 500, 1000, 2000]
    
    # empty list for change
    change = [0, 0, 0, 0, 0, 0, 0, 0]
    
    # define values used later
    total = 0
    
    # iterates through user payment input to calc total in cents
    n = 0
    for n in range(8):
        total =+ (total + refAmount[n]*payment[n])
        n =+ 1
    return total/100
    
