'''@file        encoder_task.py
@brief          Interface with encoder driver and report motor positions
@author         Mark Iknoian. 
@date           6/9/21
'''
import cotask
import task_share
import shares
from pyb import Pin
from pyb import Timer
from encoderDriver import encoderDriver
import utime


def encodertaskRun():
    
    # encoder object creation
 
    encTimer1 = Timer(4, prescaler = 0, period = 65535)
    
    pin1 = Pin(Pin.cpu.B6)
    
    pin2 = Pin(Pin.cpu.B7)
    
    enc1 = encoderDriver(encTimer1,pin1,1,pin2,2)

    encTimer2 = Timer(8, prescaler = 0, period = 65535)
    
    pin3 = Pin(Pin.cpu.C6)
    
    pin4 = Pin(Pin.cpu.C7)
    
    enc2 = encoderDriver(encTimer2,pin3,1,pin4,2)
    
    # radius of lever arm
    r_m = 60
    
    # length of pushrod
    l_r = 52
    
    
    while True:
        
        # update both encoder objects
        enc1.update()
        enc2.update()
        
        # convert to radians from ticks and divide by period for angular velocity
        enc_pos_x = enc1.get_position()*(2*3.142)/4000
        enc_pos_xd = enc1.get_delta()*(2*3.142)/(4000*.020)        
        enc_pos_y = enc2.get_position()*(2*3.142)/4000
        enc_pos_yd = enc2.get_delta()*(2*3.142)/(4000*.020) 
        
        # multiple by length ratio
        theta_x = enc_pos_x*(-r_m/l_r)
        theta_xd = enc_pos_xd*(-r_m/l_r)
        theta_y = enc_pos_y*(-r_m/l_r)
        theta_yd = enc_pos_yd*(-r_m/l_r) 
        
        # update variable
        shares.theta_x.put(theta_x)
        shares.theta_xd.put(theta_xd)
        shares.theta_y.put(theta_y)
        shares.theta_yd.put(theta_yd)
        
        # zero encoder position from UI input
        if shares.enc_zero.get() == 1:
            enc1.set_position(0)
            enc2.set_position(0)
            shares.enc_zero.put(0)
        
        yield (0)    


# setup task for scheduler
encoderTask = cotask.Task(encodertaskRun, name = 'Encoder Task', priority = 1, period = 4, profile = True, trace = False)

# send task to scheduler
cotask.task_list.append(encoderTask)