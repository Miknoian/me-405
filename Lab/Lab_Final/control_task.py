'''@file        control_task.py
@brief          Controller task for ball balancing system
@details        Takes state variable input and provides motor duty % for
                each axis based on provided gains and system constants.
@author         Mark Iknoian. 
@date           6/9/21
'''
import cotask
import task_share
import shares


def controltaskRun():
    
    # gains for each axis controller
    Kx = [-4, -400, -.1, -0] 
    Ky = [-.9, -190, -.03, -0] 
    
    # empty lists to populate
    K_px = [0,0,0,0]
    K_py = [0,0,0,0]
    
    # motor resistance from specs
    R = 2.21
    
    # motor gain from specs (TL*R/V_max)
    K_t = 13.8
    
    # motor voltage
    V_dc = 12
     
    # gains for torque eq
    for n in range(len(Kx)):
        K_px[n] = -Kx[n]*(R*100)/(4*K_t*V_dc) 

    for n in range(len(Ky)):
        K_py[n] = -Ky[n]*(R*100)/(4*K_t*V_dc) 

    
    while True:
        
        # only run when contact with touchpad 
        if shares.ball_z.get() == 1:
        
            # get all the current state
            x = -shares.ball_x.get()
            xd = -shares.ball_xd.get()
            
            xth = shares.theta_x.get()
            xthd = -shares.theta_xd.get()
            
            y = shares.ball_y.get()
            yd = shares.ball_yd.get()
            
            yth = shares.theta_y.get()
            ythd = -shares.theta_yd.get()
           
            # find duty from K prime and state
            x_duty = K_px[0]*y + K_px[1]*xth + K_px[2]*yd + K_px[3]*xthd
            y_duty = K_py[0]*x + K_py[1]*yth + K_py[2]*xd + K_py[3]*ythd
            
            # update duty
            shares.pwm_x.put(x_duty)
            shares.pwm_y.put(y_duty)
            
            # testing stuff 
            # print('{:}, {:}, {:}, {:}, {:}'.format(x_duty,y,yd,xth*57.30,xthd*57.30))
            # print('{:}, {:}, {:}, {:}, {:}'.format(y_duty,x,xd,yth*57.30,ythd*57.30))
            #print('y axis  {:}, {:}, {:}, {:}, {:}'.format(y_duty,x*K_py[0],yth*K_py[1],xd*K_py[2],ythd*K_py[3]))
            #print('x axis  {:}, {:}, {:}, {:}, {:}'.format(x_duty,y*K_px[0],xth*K_px[1],yd*K_px[2],xthd*K_px[3]))

        yield (0)    

# setup task for scheduler
global controlTask
controlTask = cotask.Task(controltaskRun, name = 'Control Task', priority = 1, period = 4, profile = True, trace = False)

# send task to scheduler
cotask.task_list.append(controlTask)