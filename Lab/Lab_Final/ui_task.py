'''@file        ui_task.py
@brief          User interface in REPL for task system
@author         Mark Iknoian. 
@date           6/9/21
'''
from pyb import USB_VCP
import cotask
import task_share
import shares
import print_task

def uitaskRun():
    
    print("'e' to enable motors, 'd' to disable motors, 'c' to clear faults, 'z' to zero encoders, and 'h' to export data")
    
    # setup comms
    comms = USB_VCP()
    
    while True:
        
        # User inputs are handled through updating a boolean in shares.py. Once updated,
        # relevant tasks scan for changes and respond accordingly.
        
        # this statements checks for current user inputs and updates corresponding var
        if comms.any():
        
            userInput = comms.read(1).decode()
            
            if userInput == 'e':
                
                print_task.put('e pressed, enabling motor\r\n')
                
                shares.motor_enable.put(1)
                userInput = 0
                
            if userInput == 'c':
                
                shares.fault_clear.put(1)
                userInput = 0
                
            if userInput == 'd':
                
                print_task.put('d pressed, disabling motor\r\n')
            
                shares.motor_disable.put(1)
                userInput = 0 
            
            if userInput == 'z':
                
                print_task.put('z pressed, zeroing encoders\r\n')
                
                shares.enc_zero.put(1)
                userInput = 0 
            
            if userInput == 'h':
                
                print_task.put('h pressed, exporting data\r\n')
                
                shares.export_data.put(1)
                userInput = 0 
                
            else:
                userInput = 0
            
        yield (0)     

# setup task for scheduler
uiTask = cotask.Task(uitaskRun, name = 'UI Task', priority = 1, period = 35, profile = True, trace = False)

# send task to scheduler
cotask.task_list.append(uiTask)