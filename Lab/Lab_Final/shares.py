'''@file        shares.py
@brief          Creates task_share object for all shared variables
@author         Mark Iknoian. 
@date           6/9/21
'''
import task_share

pwm_x = task_share.Share('f', thread_protect = True, name = "pwm x")
pwm_x.put(0)

pwm_y = task_share.Share('f', thread_protect = True, name = "pwm y")
pwm_y.put(0)

fault_status = task_share.Share('i', thread_protect = True, name = "fault status")
fault_status.put(0)

fault_clear = task_share.Share('i', thread_protect = True, name = "fault clear")
fault_clear.put(0)

motor_enable = task_share.Share('i', thread_protect = True, name = "motor enable")
motor_enable.put(0)

motor_disable = task_share.Share('i', thread_protect = True, name = "motor disable")
motor_disable.put(0)

enc_zero = task_share.Share('i', thread_protect = True, name = "encoder zero")
enc_zero.put(0)

ball_x = task_share.Share('f', thread_protect = True, name = "ball x pos")
ball_x.put(0)

ball_xd = task_share.Share('f', thread_protect = True, name = "ball x velocity")
ball_xd.put(0)

ball_y = task_share.Share('f', thread_protect = True, name = "ball y pos")
ball_y.put(0)

ball_yd = task_share.Share('f', thread_protect = True, name = "ball y velocity")
ball_yd.put(0)

ball_z = task_share.Share('i', thread_protect = True, name = "ball z pos")
ball_z.put(0)

theta_x = task_share.Share('f', thread_protect = True, name = "theta x")
theta_x.put(0)

theta_xd = task_share.Share('f', thread_protect = True, name = "x angular velocity")
theta_xd.put(0)

theta_y = task_share.Share('f', thread_protect = True, name = "theta y")
theta_y.put(0)

theta_yd = task_share.Share('f', thread_protect = True, name = "y angular velocity")
theta_yd.put(0)

export_data = task_share.Share('i', thread_protect = True, name = "data export")
export_data.put(0)