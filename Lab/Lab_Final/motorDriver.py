'''@file        motorDriver.py
@brief          Hold class to create DRV8847 driver object
@author         Mark Iknoian. 
@date           6/9/21
'''
from pyb import Pin
from pyb import Timer
from pyb import ExtInt
from pyb import udelay
class DRV8847:
    ''' @brief          Implements class for DRV8847 drivers.
        @details        Handles object creation and associated motor IC features
    '''

    def __init__ (self, nSLEEP_pin, nFAULT_pin):
        ''' @brief              Constructs a motorDriver.DRV8847 object.
            @details            This constructor takes in sleep and fault pins
                                to create an object for interfacing with motor IC
            @param nSLEEP_pin   number of timer to use
            @param nFAULT_pin   specifies what counts as a "Tick"
        '''
        
        self.nSLEEP_pin = nSLEEP_pin
        
        self.nFAULT_pin = nFAULT_pin
        
        self.fault_status = False
        
        # trigger interupt when fault pin goes low
        self.motInt = ExtInt(self.nFAULT_pin, mode=ExtInt.IRQ_FALLING, pull=Pin.PULL_UP, callback=self.fault_CB) 
            

    
    def enable (self):
        '''
        @brief               Enable all motors on IC
        '''    
        if self.fault_status == False:
            
            # disable interupt briefly as to not trigger on initialization
            
            self.motInt.disable()
            self.nSLEEP_pin.high()
            udelay(5)
            self.motInt.enable()
        else:
            print('Fault Detected! Clear Before Enabling!')

    def disable (self):
        '''
        @brief               Disable all motors on IC
        '''        
        self.nSLEEP_pin.low()

    def fault_CB (self, IRQ_src):
        '''
        @brief              Disable motors when IRQ is triggered
        @param IRQ_src      Result of pyb ExtInt callback
        '''        
        self.disable()
        self.fault_status = True
        print('Fault Detected! Use .clear_fault() to Retry!')
    
    def clear_fault(self):
        '''
        @brief               Clears motor fault
        ''' 
        self.fault_status = False
        print('Fault Cleared!')

    def channel (self, INx_pin, INy_pin, INxy_timer, channel1, channel2):
        ''' @brief              Constructs a motorDriver.DRV8847_channel object.
            @details            See DRV8847_channel for further description
        '''
        
        return DRV8847_channel(INx_pin, INy_pin, INxy_timer, channel1, channel2)
    
    def faultStatus (self):
        '''
        @brief              Disply current fault status
        ''' 
        if self.fault_status == True:
            print('Fault Detected!')
        else:
            print('No Fault Detected!')
        

class DRV8847_channel:
    ''' @brief          Implements class for DRV8847 motor control drivers.
        @details        Handles object creation and associated motor specific features
    '''

    def __init__ (self, INx_pin, INy_pin, INxy_timer, channel1, channel2):
        ''' @brief              Constructs a motorDriver.DRV8847_channel object.
            @details            This constructor takes in associated HW pins and timer object
                                to create an object for interfacing with motor specific features
            @param INx_pin   first pin of motor
            @param INy_pin   second pin of motor
            @param INxy_timer  Timer used for PWM
            @param channel1   1st channel number for timer channel
            @param channel2   2nd channel number for timer channel
        '''
        
        self.INx_pin = INx_pin
        
        self.INy_pin = INy_pin
        
        self.INxy_timer = INxy_timer
        
        self.channel1 = channel1
        
        self.channel2 = channel2
        
        self.tch1 = self.INxy_timer.channel(self.channel1, pin = self.INx_pin, mode = Timer.PWM)
        
        self.tch2 = self.INxy_timer.channel(self.channel2, pin = self.INy_pin, mode = Timer.PWM)
        
    def set_duty (self, duty):
        '''
        @brief              Sets motor duty %
        @param duty         Desired duty % to set motor
        ''' 
        self.duty = duty
        
        # check if duty is positive 
        if self.duty > 0:
            if self.duty > 100:
                self.tch2.pulse_width_percent(0)                
                self.tch1.pulse_width_percent(100)
            else:
                self.tch2.pulse_width_percent(0)
                self.tch1.pulse_width_percent(self.duty)

        
        # check if reverse dirrection requested
        else:    
            if self.duty < -100:
                self.tch1.pulse_width_percent(0)
                self.tch2.pulse_width_percent(100)
            else:
                self.tch1.pulse_width_percent(0)                
                self.tch2.pulse_width_percent(abs(self.duty))     

        
if __name__ == "__main__":
    
    sleepPin = Pin(Pin.cpu.A15)
    
    faultPin = Pin(Pin.cpu.B2)
    
    in1Pin = Pin(Pin.cpu.B4)
    
    in2Pin = Pin(Pin.cpu.B5)
    
    in3Pin = Pin(Pin.cpu.B0)
    
    in4Pin = Pin(Pin.cpu.B1)
    
    motTimer = Timer(3, freq = 20000)
    
    motIC = DRV8847(sleepPin, faultPin)
    
    motIC.enable()

    mot1 = motIC.channel(in1Pin,in2Pin,motTimer,1,2)
    
    mot1.set_duty(50)
    
    mot2 = motIC.channel(in3Pin,in4Pin,motTimer,3,4)
    
    mot2.set_duty(50)