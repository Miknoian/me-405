'''@file        encoderDriver.py
@brief          Contains class for encoder drivers
@details        Holds class to create drivers that can send/set encoder position
@author         Mark Iknoian. 
@date           6/9/21
'''

import utime
from pyb import Timer
from pyb import Pin

class encoderDriver:
    ''' @brief          Implements class for encoder drivers
        @details        Class looks at encoder input to generate an absolute position.
                        Considers reading validity for overflow. Can also set position.
    '''

    def __init__(self, timer, pin1, chnl1 , pin2, chnl2):
        ''' @brief          Constructs a encoderTask.encoderTask object.
            @details        This constructor takes frequency parameters, encoder
                            pins/channels and creates a HW driver
            @param timerNum     number of timer to use
            @param prescaler    specifies what counts as a "Tick"
            @param period       value encoder will roll over at (<16 bit)
            @param pin1         specified input pin1
            @param chl1         specified channel in timer
            @param pin2         specified input pin2
            @param chl2         specified channel in timer
        '''
        
        self.timer = timer
        
        self.pin1 = pin1

        self.chnl1 = chnl1
                
        self.pin2 = pin2
        
        self.chnl2 = chnl2
            
        # create channel in timer object for encoders
        self.timer.channel(self.chnl1, pin=self.pin1, mode=Timer.ENC_AB)
        self.timer.channel(self.chnl2, pin=self.pin2, mode=Timer.ENC_AB)
        
        self.delta = 0
        
        self.currentPos = 0
        
        self.runningPos = 0
        
        self.period = self.timer.period()
  
    def update(self):
        '''
        @brief               Updates encoder position for object
        '''
        self.delta = self.timer.counter() - self.currentPos
        self.currentPos = self.timer.counter()
        if self.delta > self.period/2:
            self.runningPos += self.delta - self.period
        elif self.delta < -self.period/2:
            self.runningPos += self.delta + self.period
        else:
            self.runningPos += self.delta
        
        
    def get_position(self):
        '''
        @brief              Returns current encoder position
        @return             Current encoder positon
        '''
        return int(self.runningPos)
        
    def set_position(self,setPos):
        '''
        @brief              Set current encoder position
        '''
        self.runningPos = setPos
        
    def get_delta(self):
        '''
        @brief              Returns previous tick delta
        @return             previous tick delta
        '''
        return self.delta

if __name__ == "__main__":
    
    encTimer1 = Timer(4, prescaler = 0, period = 65535)
    
    pin1 = Pin(Pin.cpu.B6)
    
    pin2 = Pin(Pin.cpu.B7)
    
    enc1 = encoderDriver(encTimer1,pin1,1,pin2,2)

    encTimer2 = Timer(8, prescaler = 0, period = 65535)
    
    pin3 = Pin(Pin.cpu.C6)
    
    pin4 = Pin(Pin.cpu.C7)
    
    enc2 = encoderDriver(encTimer2,pin3,1,pin4,2)
    
    while True:
        enc1.update()
        enc2.update()
        print('Enc 1 Pos:' + str(enc1.get_position()) + '   Enc 2 Pos:' + str(enc2.get_position()))
    
    
    

    

        
        

    


