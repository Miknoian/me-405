'''@file        touch_task.py
@brief          Interfaces with touch drivers to return ball position
@details        Updates touchpad position variables
@author         Mark Iknoian. 
@date           6/9/21
'''
import cotask
import task_share
import shares
from pyb import Pin
from touchDriver import touchDriver
import print_task


def touchtaskRun():
    
    # define variables related to touchpad
 
    ym = Pin(Pin.cpu.A0)
    xm = Pin(Pin.cpu.A1)
    yp = Pin(Pin.cpu.A6)
    xp = Pin(Pin.cpu.A7)
    xCenter = 2013
    xHigh = 3755
    xLow = 315
    yCenter = 2160
    yHigh = 3663
    yLow = 625
    xLength = 169.9
    yLength = 94.5
    
    # touchPanel object creation
    touchPanel = touchDriver(ym,xm,yp,xp,xCenter,xHigh,xLow,yCenter,yHigh,yLow,xLength,yLength)
    
    while True:
        
        # get previous position for ball velocities
        
        x0 = shares.ball_x.get()
        y0 = shares.ball_y.get()
        
        # full scan of all coordinates
        
        ball_pos = touchPanel.fullScan()
        
        # update each individual coordinate
        
        shares.ball_x.put(ball_pos[0])
        shares.ball_y.put(ball_pos[1])
        shares.ball_z.put(ball_pos[2])
        
        # update velocities
        
        shares.ball_xd.put((ball_pos[0]-x0)/.02)
        shares.ball_yd.put((ball_pos[1]-y0)/.02)
        
           
        yield (0)    


# setup task for scheduler
touchTask = cotask.Task(touchtaskRun, name = 'Touch Task', priority = 1, period = 4, profile = True, trace = False)

# send task to scheduler
cotask.task_list.append(touchTask)