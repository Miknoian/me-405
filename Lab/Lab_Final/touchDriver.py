'''@file        touchDriver.py
@brief          Holds class to create touchpad driver object
@author         Mark Iknoian. 
@date           6/9/21
'''
from pyb import Pin
from pyb import ADC
from pyb import udelay
import utime

class touchDriver:
    ''' @brief          Implements class for touchpad drivers.
        @details        Handles object creation and associated touchpad features
    '''    
    def __init__(self, pin_ym, pin_xm, pin_yp, pin_xp, xCenter, xHigh, xLow, yCenter, yHigh, yLow, xLength, yLength):
        ''' @brief              Constructs a touchDriver.touchDriver object.
            @details            This constructor takes in touch pad pins and calibration
                                information
            @param pin_ym       ym board pin
            @param pin_xm       xm board pin
            @param pin_yp       yp board pin
            @param pin_xp       xp board pin
            @param xCenter      x position corresponding to physical x center
            @param xHigh        x position corresponding to top of physical length measurement
            @param xlow         x position corresponding to bottom of physical length measurement  
            @param yCenter      y position corresponding to physical y center
            @param yHigh        y position corresponding to top of physical length measurement
            @param ylow         y position corresponding to bottom of physical length measurement
            @param xSlope       "slope" of equation for physical touchpad position
            @param ySlope       "slope" of equation for physical touchpad position
        '''       
        self.pin_ym = pin_ym
        
        self.pin_xm = pin_xm
        
        self.pin_yp = pin_yp
        
        self.pin_xp = pin_xp
        
        self.xCenter = xCenter
        
        self.xHigh = xHigh
        
        self.xLow = xLow
        
        self.yCenter = yCenter
        
        self.yHigh = yHigh
        
        self.yLow = yLow
        
        self.xLength = xLength
        
        self.yLength = yLength
        
        self.xSlope = (self.xHigh - self.xLow)/xLength
        
        self.ySlope = (self.yHigh - self.yLow)/yLength
        
    def xScan(self):
        ''' @brief          Scan x position
            @return         X position        
        ''' 
        
        # configure pins
        self.pin_ym.init(Pin.IN)
        self.pin_xm.init(Pin.OUT_PP, value=0)
        self.pin_yp.init(Pin.IN)
        self.pin_xp.init(Pin.OUT_PP, value=1)
        
        # wait for pins to initialize
        udelay(4)
        
        # read pin voltage
        self.xRead = ADC(self.pin_ym)
        
        # find realworld position on touch pad from center origin
        self.xActual = self.xRead.read()/self.xSlope - self.xCenter/self.xSlope
        
        return self.xActual

    def yScan(self):
        ''' @brief          Scan y position
            @return         Y position        
        '''         
        self.pin_ym.init(Pin.OUT_PP, value=0)
        self.pin_xm.init(Pin.IN)
        self.pin_yp.init(Pin.OUT_PP, value=1)
        self.pin_xp.init(Pin.IN)
        
        udelay(4)
        
        self.yRead = ADC(self.pin_xm)
        
        self.yActual = self.yRead.read()/self.ySlope - self.yCenter/self.ySlope
        
        return self.yActual 

    def zScan(self):
        ''' @brief          Scan z position
            @return         Z position        
        '''         
        self.pin_ym.init(Pin.IN)
        self.pin_xm.init(Pin.OUT_PP, value=0)
        self.pin_yp.init(Pin.OUT_PP, value=1)
        self.pin_xp.init(Pin.IN)
        
        udelay(4)
        
        self.zRead = ADC(self.pin_ym)
        
        if self.zRead.read() < 4070:
            self.zActual = 1
        else:
            self.zActual = 0
        
        return self.zActual
    
    def fullScan(self):
        ''' @brief          Scan all positions
            @return         X,Y,Z position        
        ''' 
        
        # scan y,x,z as to not configure pins unnecesarily 
        
        self.pin_ym.init(Pin.OUT_PP, value=0)
        self.pin_xm.init(Pin.IN)
        self.pin_yp.init(Pin.OUT_PP, value=1)
        self.pin_xp.init(Pin.IN)
        
        udelay(4)
        
        self.yRead = ADC(self.pin_xm)
        
        self.yActual = self.yRead.read()/self.ySlope - self.yCenter/self.ySlope
        
        self.pin_ym.init(Pin.IN)
        self.pin_xm.init(Pin.OUT_PP, value=0)
        
        udelay(4)
        
        self.zRead = ADC(self.pin_ym)
        
        if self.zRead.read() < 4070:
            self.zActual = 1
        else:
            self.zActual = 0
        
        self.pin_yp.init(Pin.IN)
        self.pin_xp.init(Pin.OUT_PP, value=1)
        
        udelay(4)
        
        self.xRead = ADC(self.pin_ym)
        
        self.xActual = self.xRead.read()/self.xSlope - self.xCenter/self.xSlope        

        return(self.xActual,self.yActual,self.zActual)


if __name__=='__main__':
    
    ym = Pin(Pin.cpu.A0)
    xm = Pin(Pin.cpu.A1)
    yp = Pin(Pin.cpu.A6)
    xp = Pin(Pin.cpu.A7)
    xCenter = 2013
    xHigh = 3755
    xLow = 315
    yCenter = 2160
    yHigh = 3663
    yLow = 625
    xLength = 169.9
    yLength = 94.5
    
    touchPanel = touchDriver(ym,xm,yp,xp,xCenter,xHigh,xLow,yCenter,yHigh,yLow,xLength,yLength)
    
    try:
        while True:
            
            startTime = utime.ticks_us()
            position = touchPanel.fullScan()
            endTime = utime.ticks_us()
            elapsedTime = utime.ticks_diff(endTime,startTime)
            print(str(position) + ' With a delay of {:} us'.format(elapsedTime))
            utime.sleep(.25)
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
        
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')
        


