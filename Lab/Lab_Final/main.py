'''@file        labFF_main.py
@brief          Runs scheduler for task system
@details        Main program loop to call and execute cotask object.
@author         Mark Iknoian. 
@date           6/9/21
'''

# import all tasks desired to run

from micropython import const, alloc_emergency_exception_buf
import gc
import cotask
import motor_task
import ui_task
import shares
import utime
import task_share
import touch_task
import encoder_task
import control_task
import data_task


# Run the scheduler with the chosen scheduling algorithm. Quit if any 
# character is sent through the serial port
while True:
    cotask.task_list.pri_sched()


