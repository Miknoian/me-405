'''@file        motor_task.py
@brief          Interfaces with motor driver, controller, and UI for balancing system
@details        Set new motor duty each iteration and handles motor faults.
@author         Mark Iknoian. 
@date           6/9/21
'''
import cotask
import task_share
import shares
from pyb import Pin
from pyb import Timer
from motorDriver import DRV8847
import utime

def motortaskRun():
    
    # Motor object creation
 
    sleepPin = Pin(Pin.cpu.A15)
     
    faultPin = Pin(Pin.cpu.B2)
     
    in1Pin = Pin(Pin.cpu.B4)
     
    in2Pin = Pin(Pin.cpu.B5)
     
    in3Pin = Pin(Pin.cpu.B0)
     
    in4Pin = Pin(Pin.cpu.B1)
     
    motTimer = Timer(3, freq = 20000)
     
    motIC = DRV8847(sleepPin, faultPin)
     
    motIC.enable()
    
    mot1 = motIC.channel(in1Pin,in2Pin,motTimer,1,2)
     
    mot2 = motIC.channel(in3Pin,in4Pin,motTimer,3,4)
    
    while True:
        
        # update duty
        mot1.set_duty(shares.pwm_x.get())
        mot2.set_duty(shares.pwm_y.get())
        
        # ui related functions
        
        # check if ui calls for enable
        if shares.motor_enable.get() == 1:
            motIC.enable()
            shares.motor_enable.put(0)
        
        # check if ui calls for disable
        if shares.motor_disable.get() == 1:
            motIC.disable()
            shares.motor_disable.put(0)
        
        # check if ui calls for fault clear
        if shares.fault_clear.get() == 1:
            motIC.clear_fault()
            shares.fault_clear.put(0)
           
        yield (0)    


# setup task for scheduler
motorTask = cotask.Task(motortaskRun, name = 'Motor Task', priority = 1, period = 4, profile = True, trace = False)

# send task to scheduler
cotask.task_list.append(motorTask)
    
