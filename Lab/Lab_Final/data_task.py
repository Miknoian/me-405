'''@file        data_task.py
@brief          Data collecting task for balance system
@details        Collects state and duty for y axis controller. Prints to .csv
@author         Mark Iknoian. 
@date           6/9/21
'''
import cotask
import task_share
import shares
import array


def datataskRun():
    
    # define arrays for state variables
    x = array.array('f')
    xd = array.array('f')
    thy = array.array('f')
    thyd = array.array('f')
    
    # and duty output
    duty = array.array('f')
    
    
    while True:
        
        # add current state to array
        x.append(-shares.ball_x.get())
        xd.append(-shares.ball_xd.get())
        thy.append(shares.theta_y.get())
        thyd.append(-shares.theta_yd.get())
        duty.append(shares.pwm_y.get())
        
        # iterate through arrays and write to .csv
        if shares.export_data.get() == 1:
            shares.export_data.put(0)
            with open ("final_dataOut.csv", "w") as file:
                for n in range(len(x)):
                    
                    file.write("{:f},{:f},{:f},{:f},{:f}\r\n".format(x[n],xd[n],thy[n],thyd[n],duty[n]))        
           
        yield (0)    


# setup task for scheduler
dataTask = cotask.Task(datataskRun, name = 'data Task', priority = 1, period = 50, profile = True, trace = False)

# send task to scheduler
cotask.task_list.append(dataTask)

