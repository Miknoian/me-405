"""@file        collectTask.py 
@brief          Backend of ADC voltage data logging 
@details        Main file on Nucleo to log and transmit over serial the voltage
                rise in the discharged capacitor for the user button
                (Hosted at https://bitbucket.org/Miknoian/me-405/src/master/Lab/lab3/collectTask.py)
@author         Mark Iknoian. 
@date           5/6/21
"""

from pyb import UART
import pyb
import array
from time import sleep
pyb.repl_uart(None)
myuart = UART(2)

def collectTask():
    """ @brief          FSM to collect capacitor voltage rise and transmit over serial
        @details        Uses ADC to collect voltage data and transmits data
                        after it is confirmed valid.
    """
    
    state = 0
    
    # define input/ADC pin
    pinA0 = pyb.Pin (pyb.Pin.cpu.A0)
    
    A0_ADC = pyb.ADC(pinA0)
    
    # array for data storage
    dataArray = array.array ('H', (0 for index in range (4000)))
    
    # timer for ADC
    dataTimer = pyb.Timer(6, freq = 250000)
    
    while True:
        # Implement FSM using a while loop and an if statement
        # will run eternally until user presses CTRL-C
        
        if state == 0:
            
            # Handshake to confirm successful comms
            if myuart.readchar() == 104:
                myuart.write('Handshake successful!\r\n')
                state = 1       # on the next iteration, the FSM will run state 1

        elif state == 1:
            
            # Wait for user input to start data collection
            if myuart.readchar() == 115:
                myuart.write('Starting Data Collection!\r\n')
                state = 2       # on the next iteration, the FSM will run state 1
                
        elif state == 2:
            
            # read ADC voltage and confirm valid data
            A0_ADC.read_timed(dataArray, dataTimer)
            
            # make sure entire rise is captured
            if dataArray[1999] - dataArray[0] > 1000:
                state = 3
            # perform state 2 operations
            
        elif state == 3:
            
            # iterate through data and send over serial line by line
            for count,voltage in enumerate(dataArray,0):
                print(count)
                myuart.write('{:},{:}\r\n'.format(voltage,count*12e-6))
            myuart.write('end\r\n')
            state = 1
                        
        else:
            # this state shouldn't exist!
            
            pass
        
        
if __name__ == "__main__":
 
    collect = collectTask()  
 
    # generator for FSM
    try:
        while True:
            # sleep(0.1) 
            next(collect)         
       
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
            
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')



        
