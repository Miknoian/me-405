"""@file        405_lab3.py 
@brief          UI frontend of ADC capture of button capactior charging
@details        Interfaces with Nucleo over serial to initiate, record, plot, and export
                the voltage of charging capacitor for the user button over time.
                (Hosted at https://bitbucket.org/Miknoian/me-405/src/master/Lab/lab3/405_lab3.py)
                (Data available at https://bitbucket.org/Miknoian/me-405/src/master/Lab/lab3/lab3_out.csv)
@author         Mark Iknoian. 
@date           5/6/21
@image          html lab_3.png "Program FSM"
@image          html lab3_plot.png "Plot of capacitor voltage vs time. From this data, a time constant of .00038 was calculated. Compared to the predicted value of .00048 an error of 26% is found."
"""

import serial
import keyboard
import array
from matplotlib import pyplot
import numpy
import sys

global startCollect

def uiTask():
    """ @brief          Implements FSM for data collection over serial
        @details        Initiates handshake and waits for "g" keypress to start
                        data collection. Handles formating, ploting, and
                        exporting to CSV.
    """
    
    # init state
    state = 0
    
    # global vars shared between input loop/function
    global last_key
    global startCollect
    
    startCollect = 0
    
    # arrays for voltage data
    time = array.array('f')
    data = array.array('f')
    
    # start serial communication with Nucleo
    ser = serial.Serial(port='COM4',baudrate=115200,timeout=5)
    
    while True:
        # Implement FSM using a while loop and an if statement
        # will run eternally until user presses CTRL-C
         
        if state == 0:
            
            # Confirms communication with Nucleo is succesful
            ser.write('h'.encode())
            serialIn = ser.readline().decode()
            print(serialIn)
            if serialIn == 'Handshake successful!\r\n':
                state = 1
            
        elif state == 1:
            
            # wait for user input to start data collection
            if startCollect == 1:
                startCollect = 0
                ser.write('s'.encode())
                print('Starting data collection!')
                
                # close serial to prep for with statement
                ser.close()
                state = 2


        elif state == 2:
            
            # iterate through each line of serial input and strip/append to arrays
            with serial.Serial(port='COM4',baudrate=115200) as ser:
                for line in ser:
                    serialIn = ser.readline().decode()
                    if serialIn == 'end\r\n':
                        state = 3
                        break
                    print(serialIn)
                    
                    [y,t] = serialIn.strip().split(',')
                    
                    # Convert strings to floats
                    time.append(float(t))
                    data.append(float(y)/1240.9)
                    
            print('Collection successful!')
            state = 3

        elif state == 3:
            
            # plot and export to CSV
            numpy.savetxt("lab3_out.csv", (time,data), delimiter=',')
            pyplot.figure()
            pyplot.plot(time, data)
            pyplot.xlabel('Time (s)')
            pyplot.ylabel('Voltage (V)')
            pyplot.title('ADC Voltage vs Time')
            
            # find lower time bound for graph
            for count,voltage in enumerate(data,0):
                if data[count] > .1:
                    xstart = time[count - 50]
                    break
                
            # find upper time bound for graph
            for count,voltage in enumerate(data,0):
                if data[count] > 3.2:
                    xend = time[count + 250]
                    break            
            pyplot.axis([xstart,xend,0,3.3])
            pyplot.show()
            print('Output successful!')
            sys.exit()
            
        else:
            # this state shouldn't exist!
            pass
        
        # stop until next call
        yield(state)

def kb_cb(key):
    ''' @brief          Callback function for keyboard input
        @details        Takes user key input used for FSM
        @param key      User key input
    '''
    global last_key
    last_key = key.name

if __name__ == "__main__":

    taskIterate = uiTask()

    keyboard.on_release_key("g", callback=kb_cb)
    keyboard.on_release_key("q", callback=kb_cb) 
    # reset key input
    last_key = ''

    # generator for FSM
    try:
        while True:
            next(taskIterate)
            
            # check key input provided by callback function
            if last_key == 'g':
                last_key = ''
                startCollect = 1
            elif last_key == 'q':
                print("'q' detected. Quitting...")
                keyboard.unhook_all ()
                break 
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
        
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')